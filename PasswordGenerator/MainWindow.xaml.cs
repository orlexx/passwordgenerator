﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PasswordGenerator
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Generate_Button_Click(object sender, RoutedEventArgs e)
        {
            int pw_length;
            string password = "";
            bool isNumeric = int.TryParse(PW_Length_tb.Text, out pw_length);
            if(pw_length < 31 && isNumeric)
            {
                Random random = new Random();
                for(int i = 0; i < pw_length; i++)
                {                    
                    int randomInt = random.Next(1, 88);
                    switch (randomInt)
                    {
                        case 1:
                            password += "A";
                            break;
                        case 2:
                            password += "B";
                            break;
                        case 3:
                            password += "C";
                            break;
                        case 4:
                            password += "D";
                            break;
                        case 5:
                            password += "E";
                            break;
                        case 6:
                            password += "F";
                            break;
                        case 7:
                            password += "G";
                            break;
                        case 8:
                            password += "H";
                            break;
                        case 9:
                            password += "I";
                            break;
                        case 10:
                            password += "J";
                            break;
                        case 11:
                            password += "K";
                            break;
                        case 12:
                            password += "L";
                            break;
                        case 13:
                            password += "M";
                            break;
                        case 14:
                            password += "N";
                            break;
                        case 15:
                            password += "O";
                            break;
                        case 16:
                            password += "P";
                            break;
                        case 17:
                            password += "Q";
                            break;
                        case 18:
                            password += "R";
                            break;
                        case 19:
                            password += "S";
                            break;
                        case 20:
                            password += "T";
                            break;
                        case 21:
                            password += "U";
                            break;
                        case 22:
                            password += "V";
                            break;
                        case 23:
                            password += "W";
                            break;
                        case 24:
                            password += "X";
                            break;
                        case 25:
                            password += "Y";
                            break;
                        case 26:
                            password += "Z";
                            break;
                        case 27:
                            password += "a";
                            break;
                        case 28:
                            password += "b";
                            break;
                        case 29:
                            password += "c";
                            break;
                        case 30:
                            password += "d";
                            break;
                        case 31:
                            password += "e";
                            break;
                        case 32:
                            password += "f";
                            break;
                        case 33:
                            password += "g";
                            break;
                        case 34:
                            password += "h";
                            break;
                        case 35:
                            password += "i";
                            break;
                        case 36:
                            password += "j";
                            break;
                        case 37:
                            password += "k";
                            break;
                        case 38:
                            password += "m";
                            break;
                        case 39:
                            password += "n";
                            break;
                        case 40:
                            password += "o";
                            break;
                        case 41:
                            password += "p";
                            break;
                        case 42:
                            password += "q";
                            break;
                        case 43:
                            password += "r";
                            break;
                        case 44:
                            password += "s";
                            break;
                        case 45:
                            password += "t";
                            break;
                        case 46:
                            password += "u";
                            break;
                        case 47:
                            password += "v";
                            break;
                        case 48:
                            password += "w";
                            break;
                        case 49:
                            password += "x";
                            break;
                        case 50:
                            password += "y";
                            break;
                        case 51:
                            password += "z";
                            break;
                        case 52:
                            password += "1";
                            break;
                        case 53:
                            password += "2";
                            break;
                        case 54:
                            password += "3";
                            break;
                        case 55:
                            password += "4";
                            break;
                        case 56:
                            password += "5";
                            break;
                        case 57:
                            password += "6";
                            break;
                        case 58:
                            password += "7";
                            break;
                        case 59:
                            password += "8";
                            break;
                        case 60:
                            password += "9";
                            break;
                        case 61:
                            password += "0";
                            break;
                        case 62:
                            password += "!";
                            break;
                        case 63:
                            password += "\"";
                            break;
                        case 64:
                            password += "+";
                            break;
                        case 65:
                            password += "$";
                            break;
                        case 66:
                            password += "%";
                            break;
                        case 67:
                            password += "&";
                            break;
                        case 68:
                            password += "/";
                            break;
                        case 69:
                            password += "(";
                            break;
                        case 70:
                            password += ")";
                            break;
                        case 71:
                            password += "=";
                            break;
                        case 72:
                            password += "?";
                            break;
                        case 73:
                            password += "*";
                            break;
                        case 74:
                            password += "'";
                            break;
                        case 75:
                            password += ":";
                            break;
                        case 76:
                            password += ";";
                            break;
                        case 77:
                            password += "-";
                            break;
                        case 78:
                            password += ".";
                            break;
                        case 79:
                            password += ",";
                            break;
                        case 80:
                            password += "#";
                            break;
                        case 81:
                            password += "@";
                            break;
                        case 82:
                            password += "\\";
                            break;
                        case 83:
                            password += "{";
                            break;
                        case 84:
                            password += "}";
                            break;
                        case 85:
                            password += "[";
                            break;
                        case 86:
                            password += "]";
                            break;
                        case 87:
                            password += "~";
                            break;
                    }
                }
                Password_TextBlock.Text = password;
            }
        }
    }
}
